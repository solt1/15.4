﻿#include <iostream>

void PrintNumbers(int n, bool isOdd) {
    int start = isOdd ? 1 : 2; // Начальное значение счетчика в зависимости от флага isOdd
    int step = isOdd ? 2 : 2; // Шаг цикла в зависимости от флага isOdd
    for (int i = start; i <= n; i += step) {
        std::cout << i << " ";
    }
    std::cout << std::endl;
}

int main() {
    const int N = 10; // Задайте значение N
    std::cout << "All even numbers from 0 to " << N << ": ";
    PrintNumbers(N, false); // Выводим все четные числа от 0 до N
    std::cout << "All odd numbers from 0 to " << N << ": ";
    PrintNumbers(N, true); // Выводим все нечетные числа от 0 до N

    return 0;
}
